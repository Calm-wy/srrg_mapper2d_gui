
//Qt
#include <QApplication>
#include <QHBoxLayout>

//srrg includes
#include "srrg_system_utils/system_utils.h"
#include "srrg_messages/laser_message.h"
#include "srrg_messages/message_reader.h"

#include "tracker_viewer.h"
#include "simple_graph_viewer.h"
#include "mapper2d.h"
#include "laser_message_tracker.h"
#include "laser_message_tracker_params.h"
#include "mapper2d_params.h"


using namespace srrg_mapper2d_gui;
using namespace srrg_scan_matcher_gui;
using namespace srrg_mapper2d;
using namespace srrg_scan_matcher;
using namespace srrg_core;
using namespace g2o;

bool use_gui = false;
bool draw_ellipse_data = false;
bool save_clouds = false;

string dumpFilename;
string outgraphFilename = "out.g2o";


const char* banner[] = {
  "srrg_mapper2d_app: a 2D mapper from a log in txtio format",
  "",
  "Usage: mapper2d_app <options>",
  "Options:",
  "-use_gui:                                          Shows the GUI",
  "-draw_ellipse_data:                                Displays the covariances ellipses",
  "-verbose:                                          Displays execution information on the terminal",
  "-log_times:                                        Logs graph optimization times",
  "-save_clouds:                                      Saves the clouds data",
  "-bpr <float>:                                      Tracker bad points ratio",
  "-iterations <int>:                                 Tracker iterations",
  "-inlier_distance <float>:                          Tracker inlier distance",
  "-min_correspondences_ratio <float>:                Tracker minimum correspondences ratio",
  "-local_map_clipping_range <float>:                 Tracker clips local map to this distance",
  "-local_map_clipping_translation_threshold <float>: Tracker clips local map when robot translates this distance",
  "-voxelize_res <float>:                             Cloud voxelization resolution",
  "-merging_distance <float>:                         Corresponding points within this distance are merged",
  "-merging_normal_angle <float>:                     Corresponding points normals within this angle are merged",
  "-projective_merge:                                 Use projective merge",
  "-tracker_damping <float>:                          Tracker damping factor",
  "-max_matching_range <float>:                       Tracker max matching range. If 0 takes the value from laser message",
  "-min_matching_range <float>:                       Tracker min matching range. If 0 takes the value from laser message",
  "-num_matching_beams <int>:                         Tracker number of matching beams. If 0 takes the value from laser message",
  "-matching_fov <float>:                             Tracker matching field of view. If 0 takes the value from laser message",
  "-frame_skip <int>:                                 Use one scan every <frame_skip> scans",
  "-laser_translation_threshold <float>:              Translation threshold to process a new scan. (0 = process all)",
  "-laser_rotation_threshold <float>:                 Rotation threshold to process a new scan. (0 = process all)",
  "-odom_weights <int,int,int>:                       Weights for odometry (x,y,theta) as initial guess separated by commas without spaces (e.g: 10,5,10)",
  "-cfinder <string>:                                 Choose between projective or nn correspondence finder",
  "-closures_inlier_threshold <float>:                Max error to consider inliers for loop closing",
  "-closures_window <int>:                            Sliding window to look for loop closures",
  "-closures_min_inliers <int>:                       Minimum number of inliers for loop closing",
  "-lcaligner_inliers_distance <float>:               Max error to consider inliers in validation of loop closures",
  "-lcaligner_min_inliers_ratio <float>:              Min inliers ratio in validation for loop closures",
  "-lcaligner_min_num_correspondences <int>:          Minimum number of correspondences in validation for loop closures",
  "-use_merger:                                       Merge vertices of the graph",
  "-vertex_translation_threshold <float>:             Translation threshold to add a new vertex",
  "-vertex_rotation_threshold <float>:                Rotation threshold to add a new vertex",
  "-o <string>:                                       Name of the file where to save the graph (e.g., out.g2o)",
  "-h:                                                Shows this help",
  0
};

LaserMessageTrackerParams* trackerparams = new LaserMessageTrackerParams;
Mapper2DParams* mapperparams = new Mapper2DParams;

void readParameters(int argc, char **argv){
  
  bool have_dumpFilename = false;

  //parsing program options

  int c = 1;
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      printBanner(banner);
      exit(0);
    } else if (!strcmp(argv[c], "-use_gui")) {
      use_gui = true;
    } else if (!strcmp(argv[c], "-draw_ellipse_data")) {
      draw_ellipse_data = true;
    } else if (!strcmp(argv[c], "-verbose")) {
      trackerparams->setVerbose(true);
      mapperparams->setVerbose(true);
    } else if (!strcmp(argv[c], "-log_times")) {
      mapperparams->setLogTimes(true);
    } else if (!strcmp(argv[c], "-save_clouds")) {
      save_clouds = true;
    } else if (!strcmp(argv[c], "-bpr")) {
      double bpr = atof(argv[++c]);
      trackerparams->setBpr(bpr);
    } else if (!strcmp(argv[c], "-iterations")) {
      int iterations = atoi(argv[++c]);
      trackerparams->setIterations(iterations);
    } else if (!strcmp(argv[c], "-inlier_distance")) {
      double inlier_distance = atof(argv[++c]);
      trackerparams->setInlierDistance(inlier_distance);
    } else if (!strcmp(argv[c], "-min_correspondences_ratio")) {
      double min_correspondences_ratio = atof(argv[++c]);
      trackerparams->setMinCorrespondencesRatio(min_correspondences_ratio);
    } else if (!strcmp(argv[c], "-local_map_clipping_range")) {
      double local_map_clipping_range = atof(argv[++c]);
      trackerparams->setLocalMapClippingRange(local_map_clipping_range);
    } else if (!strcmp(argv[c], "-local_map_clipping_translation_threshold")) {
      double local_map_clipping_translation_threshold = atof(argv[++c]);
      trackerparams->setLocalMapClippingTranslationThreshold(local_map_clipping_translation_threshold);
    } else if (!strcmp(argv[c], "-voxelize_res")) {
      double voxelize_res = atof(argv[++c]);
      trackerparams->setVoxelizeResolution(voxelize_res);
    } else if (!strcmp(argv[c], "-merging_distance")) {
      double merging_distance = atof(argv[++c]);
      trackerparams->setMergingDistance(merging_distance);
    } else if (!strcmp(argv[c], "-merging_normal_angle")) {
      double merging_normal_angle = atof(argv[++c]);
      trackerparams->setMergingNormalAngle(merging_normal_angle);
    } else if (!strcmp(argv[c], "-projective_merge")) {
      trackerparams->setProjectiveMerge(true);
    } else if (!strcmp(argv[c], "-tracker_damping")) {
      double tracker_damping = atof(argv[++c]);
      trackerparams->setTrackerDamping(tracker_damping);
    } else if (!strcmp(argv[c], "-max_matching_range")) {
      double max_matching_range = atof(argv[++c]);
      trackerparams->setMaxMatchingRange(max_matching_range);
    } else if (!strcmp(argv[c], "-min_matching_range")) {
      double min_matching_range = atof(argv[++c]);
      trackerparams->setMinMatchingRange(min_matching_range);
    } else if (!strcmp(argv[c], "-num_matching_beams")) {
      int num_matching_beams = atoi(argv[++c]);
      trackerparams->setNumMatchingBeams(num_matching_beams);
    } else if (!strcmp(argv[c], "-matching_fov")) {
      double matching_fov = atof(argv[++c]);
      trackerparams->setMatchingFov(matching_fov);
    } else if (!strcmp(argv[c], "-frame_skip")) {
      int frame_skip = atoi(argv[++c]);
      if (frame_skip <=0)
	frame_skip = 1;
      trackerparams->setFrameSkip(frame_skip);
    } else if (!strcmp(argv[c], "-laser_translation_threshold")) {
      double laser_translation_threshold = atof(argv[++c]);
      trackerparams->setLaserTranslationThreshold(laser_translation_threshold);
    } else if (!strcmp(argv[c], "-laser_rotation_threshold")) {
      double laser_rotation_threshold = atof(argv[++c]);
      trackerparams->setLaserRotationThreshold(laser_rotation_threshold);
    } else if (!strcmp(argv[c], "-odom_weights")) {
      Eigen::Vector3i odom_weights(0,0,0);
      char *str = argv[++c];
      char *pch;
      pch = strtok (str, ",");
      size_t i = 0;
      while (pch != NULL && i < 3) {
	odom_weights(i) = atoi(pch);
	pch = strtok (NULL, ",");
	i++;
      }
      if (pch != NULL || i < 3) {
	std::cerr << "Wrong odom_weights size. Type -h for help." << std::endl;
	exit(0);
      }
      trackerparams->setOdomWeights(odom_weights);
    } else if (!strcmp(argv[c], "-cfinder")) {
      std::string cfinder = std::string(argv[++c]);
      trackerparams->setCorrespondenceFinderType(cfinder);
    } else if (!strcmp(argv[c], "-closures_inlier_threshold")) {
      double closures_inlier_threshold = atof(argv[++c]);
      mapperparams->setClosuresInlierThreshold(closures_inlier_threshold);
    } else if (!strcmp(argv[c], "-closures_window")) {
      int closures_window = atoi(argv[++c]);
      mapperparams->setClosuresWindow(closures_window);
    } else if (!strcmp(argv[c], "-closures_min_inliers")) {
      int closures_min_inliers = atoi(argv[++c]);
      mapperparams->setClosuresMinInliers(closures_min_inliers);
    } else if (!strcmp(argv[c], "-lcaligner_inliers_distance")) {
      double lcaligner_inliers_distance = atof(argv[++c]);
      mapperparams->setLcalignerInliersDistance(lcaligner_inliers_distance);
    } else if (!strcmp(argv[c], "-lcaligner_min_inliers_ratio")) {
      double lcaligner_min_inliers_ratio = atof(argv[++c]);
      mapperparams->setLcalignerMinInliersRatio(lcaligner_min_inliers_ratio);
    } else if (!strcmp(argv[c], "-lcaligner_min_num_correspondences")) {
      int lcaligner_min_num_correspondences = atoi(argv[++c]);
      mapperparams->setLcalignerMinNumCorrespondences(lcaligner_min_num_correspondences);
    } else if (!strcmp(argv[c], "-use_merger")) {
      mapperparams->setUseMerger(true);
    } else if (!strcmp(argv[c], "-vertex_translation_threshold")) {
      double vertex_translation_threshold = atof(argv[++c]);
      mapperparams->setVertexTranslationThreshold(vertex_translation_threshold);
    } else if (!strcmp(argv[c], "-vertex_rotation_threshold")) {
      double vertex_rotation_threshold = atof(argv[++c]);
      mapperparams->setVertexRotationThreshold(vertex_rotation_threshold);
    } else if (!strcmp(argv[c], "-o")) {
      outgraphFilename =  std::string(argv[++c]);
    } else if (c == argc-1) {
      dumpFilename = std::string(argv[c]);
      have_dumpFilename = true;
    } else {
      std::cerr << "Unknown program option: " << argv[c] << std::endl;
      exit(0);
    }
    c++;
  }

  std::cout << std::endl;
  
  std::cout << "Launched with params: " << std::endl;
  std::cout << "use_gui " << (use_gui?"true":"false") << std::endl;
  std::cout << "draw_ellipse_data " << (draw_ellipse_data?"true":"false") << std::endl;
  std::cout << "verbose " << trackerparams->verbose() << std::endl;
  std::cout << "log_times " << mapperparams->logTimes() << std::endl;
  std::cout << "save_clouds " << (save_clouds?"true":"false") << std::endl;
  std::cout << "bpr " << trackerparams->bpr() << std::endl;
  std::cout << "iterations " << trackerparams->iterations() << std::endl;
  std::cout << "inlier_distance " << trackerparams->inlierDistance() << std::endl;
  std::cout << "min_correspondences_ratio " << trackerparams->minCorrespondencesRatio() << std::endl;
  std::cout << "local_map_clipping_range " << trackerparams->localMapClippingRange() << std::endl;
  std::cout << "local_map_clipping_translation_threshold " << trackerparams->localMapClippingTranslationThreshold() << std::endl;
  std::cout << "voxelize_res " << trackerparams->voxelizeResolution() << std::endl;
  std::cout << "merging_distance " << trackerparams->mergingDistance() << std::endl;
  std::cout << "merging_normal_angle " << trackerparams->mergingNormalAngle() << std::endl;
  std::cout << "projective_merge " << (trackerparams->projectiveMerge()?"true":"false") << std::endl;
  std::cout << "tracker_damping " << trackerparams->trackerDamping() << std::endl;
  std::cout << "max_matching_range " << trackerparams->maxMatchingRange() << std::endl;
  std::cout << "min_matching_range " << trackerparams->minMatchingRange() << std::endl;
  std::cout << "num_matching_beams " << trackerparams->numMatchingBeams() << std::endl;
  std::cout << "matching_fov " << trackerparams->matchingFov() << std::endl;
  std::cout << "frame_skip " << trackerparams->frameSkip() << std::endl;
  std::cout << "laser_translation_threshold " << trackerparams->laserTranslationThreshold() << std::endl;
  std::cout << "laser_rotation_threshold " << trackerparams->laserRotationThreshold() << std::endl;
  std::cout << "odom_weights " << trackerparams->odomWeights().transpose() << std::endl;
  std::cout << "cfinder " << trackerparams->correspondenceFinderType() << std::endl;
  std::cout << "closures_inlier_threshold " << mapperparams->closuresInlierThreshold() << std::endl;
  std::cout << "closures_window " << mapperparams->closuresWindow() << std::endl;
  std::cout << "closures_min_inliers " << mapperparams->closuresMinInliers() << std::endl;
  std::cout << "lcaligner_inliers_distance " << mapperparams->lcalignerInliersDistance() << std::endl;
  std::cout << "lcaligner_min_inliers_ratio " << mapperparams->lcalignerMinInliersRatio() << std::endl;
  std::cout << "lcaligner_min_num_correspondences " << mapperparams->lcalignerMinNumCorrespondences() << std::endl;
  std::cout << "use_merger " << (mapperparams->useMerger()?"true":"false") << std::endl;
  std::cout << "vertex_translation_threshold " << mapperparams->vertexTranslationThreshold() << std::endl;
  std::cout << "vertex_rotation_threshold " << mapperparams->vertexRotationThreshold() << std::endl;
  std::cout << "o " << outgraphFilename << std::endl;
  
  std::cout << std::endl;

  fflush(stdout);
  
  if (!have_dumpFilename){
    std::cout << std::endl;
    std::cout << "You must provide a last parameter with the log filename" << std::endl;
    exit(0);
  }
}

int main(int argc, char **argv){


  readParameters(argc,argv);

  MessageReader reader;
  reader.open(dumpFilename);
  if (!reader.good()){
    cerr << "Failed openning file: " << dumpFilename << std::endl;
    exit(0);
  }
  
  LaserMessageTracker* tracker = new LaserMessageTracker();
  SimpleGraph* sgraph = new SimpleGraph;
  Mapper2D* mapper = new Mapper2D(tracker, sgraph);

  trackerparams->setTracker(tracker);
  mapperparams->setMapper(mapper);

  trackerparams->applyParams();
  mapperparams->applyParams();
  
  QApplication* app=0;
  QWidget* mainWindow=0;
  Tracker2DViewer* tviewer=0;
  SimpleGraphViewer* sviewer=0;
  if (use_gui) {
    app=new QApplication(argc, argv);
    mainWindow = new QWidget();
    mainWindow->setWindowTitle("mapper2d_viewer");
    QHBoxLayout* hlayout = new QHBoxLayout();
    mainWindow->setLayout(hlayout);
    tviewer=new Tracker2DViewer(tracker);
    sviewer=new SimpleGraphViewer(sgraph);
    hlayout->addWidget(tviewer);
    hlayout->addWidget(sviewer);
    tviewer->init();
    mainWindow->showMaximized();
    mainWindow->show();
    tviewer->show();
    sviewer->show();
    sviewer->setDrawCovariances(draw_ellipse_data);
  }

  int prevSize = sgraph->graph()->vertices().size();
  BaseMessage* msg= reader.readMessage();
  LaserMessage* laser_msg = dynamic_cast<LaserMessage*>(msg);
  mapper->init(laser_msg);
  while (msg = reader.readMessage()) {
    laser_msg = dynamic_cast<LaserMessage*>(msg);
    mapper->compute(laser_msg);

    if (save_clouds)
      mapper->saveData();
    
    if (use_gui) {
      //Updating viewers
      int currSize = sgraph->graph()->vertices().size();
      if (currSize != prevSize || mapper->forceDisplay()){
	//only update map view if changed
	sviewer->updateGL();
	prevSize = currSize;
      }
      tviewer->updateGL();

      //Processing events
      app->processEvents();
      QKeyEvent* event=tviewer->lastKeyEvent();
      if (event) {
	if (event->key()==Qt::Key_F) {
	  tviewer->setFollowRobotEnabled(!tviewer->followRobotEnabled());
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_B) {
	  tviewer->setDrawRobotEnabled(!tviewer->drawRobotEnabled());
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_R) {
	  tracker->reset();
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_H) {
	  cerr << "HELP" << endl;
	  cerr << "R: reset tracker" << endl;
	  cerr << "B: draws robot" << endl;
	  cerr << "F: enables/disables robot following" << endl;
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_P) {//PAUSE
	  tviewer->keyEventProcessed();
	  cerr << "PAUSED. Press key 'P' to continue." << endl;
	  while (true){
	    app->processEvents();
	    event=tviewer->lastKeyEvent();
	    if (event && event->key()==Qt::Key_P){
	      tviewer->keyEventProcessed();
	      break;
	    }
	    usleep(100000);
	  }
	}
	if (event->key()==Qt::Key_O) {//SAVE
	  tviewer->keyEventProcessed();
	  sgraph->saveGraph(outgraphFilename.c_str());
	  sgraph->exportTrajectory("trajectory.g2o");
	  //sgraph->exportTrajectoryOriginalLaser("trajectoryLaser.g2o");
	  cerr << "Map saved." << endl;
	}
      }
    }
  }

  //Managing last information added
  mapper->finishMapping();
  
  std::cerr << "Saving graph... " << outgraphFilename << std::endl;
  sgraph->saveGraph(outgraphFilename.c_str());
  if (save_clouds){
    sgraph->saveClouds();
  }
  sgraph->exportTrajectory("trajectory.g2o");
  sgraph->exportStampedTrajectory("stamped_trajectory.g2o");
  sgraph->exportTrajectoryOriginalLaser("trajectoryLaser.g2o");

  std::cerr << "Finished." << std::endl;

  if (use_gui){
    app->exec();
    delete app;
  }

  
}


