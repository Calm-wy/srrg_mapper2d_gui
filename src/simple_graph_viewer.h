#pragma once
#include "srrg_core_viewers/simple_viewer.h"
#include "cloud2d_trajectory.h"
#include "simple_graph.h"

namespace srrg_mapper2d_gui {
  using namespace srrg_mapper2d;
  using namespace srrg_core;
  class SimpleGraphViewer: public srrg_core_viewers::SimpleViewer{
  public:
    SimpleGraphViewer(SimpleGraph* sgraph_ = 0);

    inline void setGraph(SimpleGraph* sgraph_) {_sgraph = sgraph_;}
    void draw();

    void drawEdges();
    void drawEdge(EdgeSE2* e);

    void drawClouds();

    void drawVertices();
    void drawVertex(VertexSE2* v);

    void drawCovariances();
    void drawCovariance(VertexSE2* v);

    inline void setDrawCovariances(bool draw_covariances) {_draw_covariances = draw_covariances;}
    inline void setDrawClouds(bool draw_clouds) {_draw_clouds = draw_clouds;}
    void setColorGraph(const Eigen::Vector3f& color);

  protected:
    srrg_mapper2d::SimpleGraph* _sgraph;
    bool _draw_covariances;
    bool _draw_clouds;
  };

}
