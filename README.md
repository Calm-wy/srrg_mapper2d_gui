# srrg_mapper2d_gui

This package implements our 2D graph SLAM system based on local maps of 2D clouds. It makes use of the [srrg_scan_matcher](https://gitlab.com/srrg-software/srrg_scan_matcher) to consider highly dynamic entities and the utilities implemented on [srrg_mapper2d](https://gitlab.com/srrg-software/srrg_mapper2d) to consider the *front-end* aspects of the graph-based SLAM system. 

## Prerequisites

This package requires:
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_core_viewers](https://gitlab.com/srrg-software/srrg_core_viewers)
* [srrg_gl_helpers](https://gitlab.com/srrg-software/srrg_gl_helpers)
* [srrg_scan_matcher](https://gitlab.com/srrg-software/srrg_scan_matcher)
* [srrg_scan_matcher_gui](https://gitlab.com/srrg-software/srrg_scan_matcher_gui)
* [srrg_mapper2d](https://gitlab.com/srrg-software/srrg_mapper2d)

This package depends on **g2o**:
* [g2o](https://github.com/RainerKuemmerle/g2o)

## Installation

Clone and compile the above mentioned packages on your catkin workspace.
For a clean installation of the `srrg` environment check [srrg_scripts](https://gitlab.com/srrg-software/srrg_scripts) using as option `packages_mapper2d.txt` during the setup.

## Applications

### mapper2d_app
Performs 2D SLAM from a txt_io file. Run it as a ROS node by typing:

```sh
$ rosrun srrg_mapper2d_gui mapper2d_app
```

From the graphical interface you can:
* Set up the **local mapper params**. Although default params usually work well, you might want to adjust local map size (`local_map_clipping_range`) or the odometry weights (`odom_weights`, the higher the more you trust on your robot's odometry) 
* Set up the **mapper params**. Increase `vertex_translation_threshold` to obtain a sparser graph or enable the vertex merging upon loop closures detections with `use_merger` for long-term graph maintenance.
* **Load** a log file in our txt_io format. Check the tool `rosbag_to_message_converter` available on [srrg_core_ros](https://gitlab.com/srrg-software/srrg_core_ros) to generate a txt_io log from a ROS bagfile.
* **Save** the output of this process into a map in **g2o** format together with the local maps as 2D clouds. Two g2o files are generated, one containing the *graph* structure (sparser) and one with the *trajectory* waypoints. Each g2o file can be converted into an occupancy grid compatible with the ROS format using the `srrg_g2o2ros` tool in [srrg_mapper2d_ros](https://gitlab.com/srrg-software/srrg_mapper2d_ros).    
* Start a **new mapping session** from a previously saved *graph* g2o file. After loading a session you can choose a new log containing the data of the new session. If the initial position is not aligned with the previous map, you can provide the initial pose through the GUI. Select the new scan with ALT+click and drag mouse to translate/rotate the scan (Right/Left button respectively).  



### srrg_cloud_graph_viewer
Visualizes the graph generated by the ``mapper2d_app``. Run it as a ROS node by typing:

```sh
$ rosrun srrg_mapper2d_gui srrg_cloud_graph_viewer_app output-graph.g2o
```


## References
Please cite the following paper to reference our work.

```
   @INPROCEEDINGS{LazaroIROS18,
     author = {M. T. L\'azaro and R. Capobianco  and G. Grisetti},
     title = {Efficient Long-term Mapping in Dynamic Environments},
     booktitle = {IEEE/RSJ International Conference on Intelligent Robots and Systems},
     month = {Oct 1-5},
     year = {2018},
     address = {Madrid, Spain}
   } 
```